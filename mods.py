import os, re, sys
import datetime
import argparse
import json

import pandas as pd
import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u

def parsing():
    parser = argparse.ArgumentParser(description='Script to create a .key file for spacecraft observations.', epilog='For bug and feature requests, please contact Giuseppe Cimo\': cimo@jive.eu\n')
    parser.add_argument('in_file', help='input coordinates file.', nargs='?')
    parser.add_argument('out_file', help='output key file.')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-r', help='Satellite is RadioAstron', action="store_true")
    group.add_argument('-v', help='Satellite is Vex', action="store_true")
    group.add_argument('-m', help='Satellite is Mex', action="store_true")
    group.add_argument('-g', help='Satellite is Gaia', action="store_true")
    group.add_argument('-b', help='Satellite is BEPICOLOMBO', action="store_true")
    group.add_argument('-j', help='Satellite is Juno', action="store_true")
    group.add_argument('-juice', help='Satellite is JUICE', action="store_true")
    group.add_argument('-o', help='Satellite is M20', action="store_true")
    group.add_argument('-a', help='Satellite is an asteroid')
    # group2.add_argument('-M', help='\'mix\' setup single pol', action="store_true")
    # group2.add_argument('-d', help='\'mix\' setup dual pol', action="store_true")
    parser.add_argument('--long', help='Experiment finishing another day', action="store_true")
    parser.add_argument('--nogap', help='No gap for RA observations', action="store_true")
    parser.add_argument('--sched', help='Run Sched to create the .vex file', action="store_true")
    parser.add_argument('-t', '--time', help='Start time of the Observations, if different than the first scan time on the spacecraft.', action="store_true")
    parser.add_argument('-s', help='Insert the scan duration (eg. 30m or 20s)')
    parser.add_argument('-S', help='Dur=20m for VEX/MEX and Dur=15s for RadioAstron', action="store_true")
    parser.add_argument('--corr', help='Create a dummy file for correlation purposes', action="store_true")
    parser.add_argument('--scan', help='Total/cumulated scan length (in minutes) for RA observations (eg. 19)')
    parser.add_argument('--nomid', help='Coordinates are NOT calculated at the middle of the scan', action="store_true")
    parser.add_argument('--setup', help='Create an ad-hoc frequency setup', action="store_true")
    parser.add_argument('--pi', help='Provide PI name (Sergei, Giuseppe, Tatiana or Guifre) --> ')
    parser.add_argument('--sumtb', help='Print out a block summary of the observations', action="store_true")
    args = parser.parse_args()
    return args


def paths():
    """
    <main_path> is the directory of the MAKEKEY program.

    <keyfile_path> is the main directory where sched-related files
                 are stored.
    """
    import os

    global wd
    wd = os.environ.get('MAKEKEY')

    main_path: str = f'{wd}'
    keyfile_path: str = f'{wd}sched_files/'

    return main_path, keyfile_path

def webgeocalc(sc, ut_start, ut_end, step, obs, ref, timesystem, state):

    sc_id, kernel_sc, apis = findKernels(sc)[3], findKernels(sc)[2], findKernels(sc)[0]

    if apis == 'ESA':
        from webgeocalc import ESA_API as API
        from webgeocalc import StateVector
        kernel_solar = 3
        kernel_leap = 4
    else:
        from webgeocalc import API
        from webgeocalc import StateVector
        kernel_solar = 1
        kernel_leap = 2

    ap=API.url
    print(f'\nUsing API: {ap}\n')
    vectors = StateVector(api = ap,
                          kernels = [kernel_sc, kernel_solar, kernel_leap],
                          intervals = [ut_start, ut_end],
                          time_step = step,
                          time_step_units = 'SECONDS',
                          target = str(sc_id),
                          observer = obs,
                          time_system = timesystem,
                          reference_frame = ref,
                          state_representation = state,
                          aberration_correction = 'NONE'
    )
    return vectors.run()


def findKernels(sc):
    sc_file = paths  ()[0]+'sc.json'
    with open(sc_file) as sc_data:
        data = json.load(sc_data)
        for i in range(len(data)):
            if sc.upper() in data[i]['Names']:
                kernel_data = data[i]['API'], data[i]['mission_id'], data[i]['kernel_id'], data[i]['NAIF ID']
    return kernel_data


def pointing(prog_dir, target, stations, ut_start, ut_end, steps):
    obs = 'EARTH'
    ref = 'J2000'
    ts = 'UTC'
    state_repr = 'RA_DEC'
    stateVectors = pd.DataFrame(webgeocalc(target, ut_start, ut_end, int(steps), obs, ref, ts, state_repr))

    filename = 'sources.coord'
    coords =  stateVectors[['DATE', 'RIGHT_ASCENSION', 'DECLINATION']]
    with open(filename, 'w') as f:
        for i in range(len(coords.DATE)):
            source = coords.at[i,'DATE'][11:13]+coords.at[i,'DATE'][14:16]+coords.at[i,'DATE'][17:19]
            coord = SkyCoord(ra=coords.at[i,'RIGHT_ASCENSION']*u.degree,dec=coords.at[i,'DECLINATION']*u.degree)
            ra = str(int(coord.ra.hms[0])).zfill(2)+':'+str(int(coord.ra.hms[1])).zfill(2)+':'+'{:0>9.6f}'.format(abs(float(coord.ra.hms[2])))
            if coord.dec < 0:
                dec = str(abs(int(coord.dec.dms[0]))).zfill(2)+':'+str(abs(int(coord.dec.dms[1]))).zfill(2)+':'+'{:0>9.6f}'.format(abs(float(coord.dec.dms[2])))
                line = "source='"+source+"' ra="+ra+" dec=-"+dec+" equinox='j2000' /\n"
            else:
                dec = str(int(coord.dec.dms[0])).zfill(2)+':'+str(int(coord.dec.dms[1])).zfill(2)+':'+'{:0>9.6f}'.format(abs(float(coord.dec.dms[2])))
                line = "source='"+source+"' ra="+ra+" dec="+dec+" equinox='j2000' /\n"
            f.write(line)
    print(f'\n Created coordinates file: {filename}\n')
    return filename


# Program that reads a coordinate files (produced by Tatiana)
# and produces a source catalog for Sched.
# A list of scans to append to the key file is also created.


def sched_ra(filename, dur):
    if os.path.exists('list.sources'):
        os.remove('list.sources')

    outfile = 'sources.coord'

    f = open(outfile, 'w')
    scan = open('list.sources', 'w')

    coords_re = re.compile('(.*source=[\'\"]?(\d+).*(ra=\w+\:\w+\:\w+\.\w+).*(dec=[-+]?\w+\:\w+\:\w+\.\w+).*)')
    for lines in open(filename):
        match = coords_re.match(lines)
        if match:
            source = match.groups()[1]
            coord_ra = match.groups()[2]
            coord_dec = match.groups()[3]
            source_lines = f'source=\'{source}\' {coord_ra} {coord_dec} equinox=\'j2000\' /\n'
            f.write(source_lines)
            scans = f"source=\'{source}\' {dur} /\n"
            scan.write(scans)

    return outfile


def sched_corr(filename, dur, sat):
    if os.path.exists('list.sources'):
        os.remove('list.sources')

    coord_count = 0
    outfile = 'sources.corr'

    f = open(outfile, 'w')
    scan = open('list.sources', 'w')

    coords_re = re.compile('(.*source=[\'\"]?(\d+).*(ra=\w+\:\w+\:\w+\.\w+).*(dec=[-+]?\w+\:\w+\:\w+\.\w+).*)')
    for lines in open(filename):
        match = coords_re.match(lines)
        if match:
            source = match.groups()[1]
            coord_ra = match.groups()[2]
            coord_dec = match.groups()[3]
            if coord_count == 0:
                source_lines = f'source=\'{sat.upper()}\' {coord_ra} {coord_dec} equinox=\'j2000\' /\n'
                f.write(source_lines)
                coord_count = 1
            scans = f'source=\'{source}\' {dur} /\n'
            scan.write(scans)

    return outfile


def scans_ra(scan_length, step):
    if os.path.exists('list.scans'):
        os.remove('list.scans')

    outfile = 'list.sources'
    scans = open('list.scans', 'w')

    times = re.compile('source=(\d+)')
    gap_time = 1

    first_scan = file(outfile).readlines()[0]
    start_time = times.match(first_scan).groups()[0]
    last_scan = file(outfile).readlines()[-1]
    last_time = times.match(last_scan).groups()[0]

    if len(start_time) == 5:
        hours, minutes, seconds = start_time[0:1], start_time[1:3], start_time[3:5]
    if len(start_time) == 6:
        hours, minutes, seconds = start_time[0:2], start_time[2:4], start_time[4:6]

    t0 = datetime.datetime(1, 1, 1, int(hours), int(minutes), int(seconds))
    tgap = t0 + datetime.timedelta(minutes=scan_length)
    tgap2 = t0 + datetime.timedelta(minutes=scan_length + gap_time)
    tgap0 = tgap2 + datetime.timedelta(seconds=step)

    def time_gap(x):
        if x == '6':
            gap = str(tgap.time())[0:2] + str(tgap.time())[3:5] + str(tgap.time())[6:8]
            gap2 = str(tgap2.time())[0:2] + str(tgap2.time())[3:5] + str(tgap2.time())[6:8]
            gap0 = str(tgap0.time())[0:2] + str(tgap0.time())[3:5] + str(tgap0.time())[6:8]
        if x == '5':
            gap = str(tgap.time())[1:2] + str(tgap.time())[3:5] + str(tgap.time())[6:8]
            gap2 = str(tgap2.time())[1:2] + str(tgap2.time())[3:5] + str(tgap2.time())[6:8]
            gap0 = str(tgap0.time())[1:2] + str(tgap0.time())[3:5] + str(tgap0.time())[6:8]
        return gap, gap2, gap0

    gap, gap2, gap0 = time_gap(str(len(start_time)))

    for lines in open(outfile):
        match = times.match(lines)
        split_line = lines
        if match:
            t = match.groups()[0]
            lines = ''.join(split_line)
            if t == last_time:
                lines = '!' + ''.join(split_line)
            elif t == gap0:
                lines = 'gap=0:00 ' + ''.join(split_line)
                tgap0 = tgap2 + datetime.timedelta(seconds=step)
                gap, gap2, gap0 = time_gap(str(len(start_time)))
            elif t >= gap and t < gap2:
                lines = '!' + ''.join(split_line)
            elif t == gap2 and t != last_time:
                lines = 'gap=1:00 ' + ''.join(split_line)
                tgap = tgap2 + datetime.timedelta(minutes=scan_length)
                tgap2 = tgap2 + datetime.timedelta(minutes=scan_length + gap_time)
                gap, gap2, gap0 = time_gap(str(len(start_time)))

            scans.write(lines)


def scans_corr(sat, scan_length):
    if os.path.exists('list.corr'):
        os.remove('list.corr')

    outfile = 'list.sources'
    scans = open('list.corr', 'w')

    times = re.compile('source=(\d+)')
    step = 60
    gap_time = 1

    first_scan = file(outfile).readlines()[0]
    start_time = times.match(first_scan).groups()[0]
    last_scan = file(outfile).readlines()[-1]
    last_time = times.match(last_scan).groups()[0]

    if len(start_time) == 5:
        hours, minutes, seconds = start_time[0:1], start_time[1:3], start_time[3:5]
    if len(start_time) == 6:
        hours, minutes, seconds = start_time[0:2], start_time[2:4], start_time[4:6]

    t0 = datetime.datetime(1, 1, 1, int(hours), int(minutes), int(seconds))
    tgap = t0 + datetime.timedelta(minutes=scan_length)
    tgap2 = t0 + datetime.timedelta(minutes=scan_length + gap_time)
    tgap0 = tgap2 + datetime.timedelta(seconds=step)

    def time_gap(x):
        if x == '6':
            gap = str(tgap.time())[0:2] + str(tgap.time())[3:5] + str(tgap.time())[6:8]
            gap2 = str(tgap2.time())[0:2] + str(tgap2.time())[3:5] + str(tgap2.time())[6:8]
            gap0 = str(tgap0.time())[0:2] + str(tgap0.time())[3:5] + str(tgap0.time())[6:8]
        if x == '5':
            gap = str(tgap.time())[1:2] + str(tgap.time())[3:5] + str(tgap.time())[6:8]
            gap2 = str(tgap2.time())[1:2] + str(tgap2.time())[3:5] + str(tgap2.time())[6:8]
            gap0 = str(tgap0.time())[1:2] + str(tgap0.time())[3:5] + str(tgap0.time())[6:8]
        return gap, gap2, gap0

    gap, gap2, gap0 = time_gap(str(len(start_time)))

    for lines in open(outfile):
        match = times.match(lines)
        split_line = lines[:14] + '\n'
        if match:
            t = match.groups()[0]
            if t == start_time:
                lines = f'gap=1:00 source={sat.upper()} dur={scan_length}:00 /\n'
                scans.write(lines)
            if t == gap2 and t != last_time:
                lines = f'gap=1:00 source={sat.upper()} dur={scan_length}:00 /\n'
                tgap = tgap2 + datetime.timedelta(minutes=scan_length)
                tgap2 = tgap2 + datetime.timedelta(minutes=scan_length + gap_time)
                gap, gap2, gap0 = time_gap(str(len(start_time)))
                scans.write(lines)


def setup():
    while True:
        nchan = input('How many channels? (4 or 8) --> ')
        if nchan == '4' or nchan == '8':
            break
        else:
            print('Please choose 4 or 8!')

    while True:
        bbfilter = input('Insert bandwidth in GHz (4, 8 or 16) --> ')
        if bbfilter == '4' or bbfilter == '8' or bbfilter == '16':
            break
        else:
            print('Please choose 4 or 8 or 16!')

    while True:
        pcal = input("PCAL 'on' or 'off' (default is OFF) --> ")
        if pcal.lower() == 'on' or pcal.lower() == 'off' or pcal == '':
            if pcal == '':
                pcal = 'off'
            else:
                pcal = pcal.lower()
            break
        else:
            print('Please write on or off')

    while True:
        freqref = input('Insert reference frequency MHz (eg. 8417.99) --> ')
        try:
            float(freqref)
            print(f'Your reference frequency is: {freqref}')
            right_freq = input('Correct? (Y,n) ')
            if right_freq == '' or right_freq[0].lower() == 'y':
                break
        except ValueError:
            print('Input error!')

    while True:
        offset = input('Insert frequency offset (eg. -10kHz or 4MHz) --> ')
        try:
            if offset[-3].lower() == 'k':
                unit: float = 1000
                break
            elif offset[-3].lower() == 'm':
                unit: float = 1
                break
        except IndexError:
            print('Unclear input! Please try again.')
        except ValueError:
            print('Unclear input! Please try again.')

    j: int = 1
    offsets = []
    offsets.append(0)
    while j < int(nchan):
        off = int(offset[:-3]) * j
        offsets.append(off / unit)
        j = j + 1

    freqoffsets = ["{0:0.2f}".format(i) for i in offsets]
    freqoff = ','.join(str(f) for f in freqoffsets)
    print(freqoff)

    header = {'nchan': nchan, 'bits': '2', 'bbfilter': bbfilter, 'freqref': freqref, 'freqoff': freqoff, 'netside': 'U', 'pcal': pcal, 'pol': 'RCP', 'format': 'vdif', 'barrel': 'roll_off'}

    setup_file = f'{nchan}ChanX{bbfilter}MHz.{freqref}'
    scans = open(setup_file, 'w')

    if nchan == '4':
        chan_file = f'{wd}sched_files/Setups/stations_setups/4.chan'
    elif nchan == '8':
        chan_file = f'{wd}sched_files/Setups/stations_setups/8.chan'

    print('\nYour frequency setup will be:\n')
    for keys in list(header.keys()):
        lines = keys + ' = ' + header.get(keys) + '\n'
        print(lines)
        scans.write(lines)

    separator = '/\n'
    scans.write(separator)

    ok = input('\nProceed? (Y/n): ')
    if ok == '' or ok[0].lower() == 'y':
        pass
    else:
        print('\nOK... Bye!')
        if os.path.exists(setup_file):
            os.remove(setup_file)
        sys.exit(1)

    for line in open(chan_file):
        scans.write(line)

    return setup_file
